Name:           antlr4
Version:        4.5.2
Release:        6
Summary:        Java parser generator
License:        BSD
URL:            http://www.antlr.org/
BuildArch:      noarch

Source0:        https://github.com/antlr/antlr4/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:  maven-local mvn(org.abego.treelayout:org.abego.treelayout.core) mvn(org.sonatype.plexus:plexus-build-api)
BuildRequires:  mvn(org.antlr:antlr3-maven-plugin) mvn(org.antlr:antlr4-maven-plugin)
BuildRequires:  mvn(org.antlr:antlr-runtime) mvn(org.antlr:ST4) mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:  mvn(org.apache.maven:maven-plugin-api) mvn(org.apache.maven:maven-project)
BuildRequires:  mvn(org.apache.maven.plugins:maven-plugin-plugin) mvn(org.apache.maven.plugin-tools:maven-plugin-annotations)
BuildRequires:  mvn(org.codehaus.plexus:plexus-compiler-api) mvn(org.sonatype.oss:oss-parent:pom:)

Provides:       %{name}-runtime = %{version}-%{release} %{name}-maven-plugin = %{version}-%{release}
Obsoletes:      %{name}-runtime < %{version}-%{release} %{name}-maven-plugin < %{version}-%{release}

%description
ANTLR is a lexer and parser generator which can manipulate text and binary files
that have specific structure or format. It provides abilities to read, process,
execute or translate these files and so are widely applied to build custom domain
specific language, tools and frameworks. In a word, ANTLR generates a parser which
can be used to walk abstract syntax tree according to your needs.

%package        help
Summary:        Documentation for antlr4
Provides:       %{name}-javadoc = %{version}-%{release}
Obsoletes:      %{name}-javadoc < %{version}-%{release}

%description    help
Documentation for antlr4.

%prep
%autosetup -p1
find -name \*.jar -delete

%pom_disable_module runtime-testsuite
%pom_disable_module tool-testsuite
%pom_remove_plugin :maven-shade-plugin tool
find -name TestPerformance.java -delete
%mvn_package :%{name}-master %{name}-runtime

%build
%mvn_build -s -f

%install
%mvn_install

%jpackage_script org.antlr.v4.Tool "" "" antlr4/antlr4:antlr3-runtime:antlr4/antlr4-runtime:stringtemplate4:treelayout %{name} true

%files          -f .mfiles-antlr4 -f .mfiles-antlr4-runtime -f .mfiles-antlr4-maven-plugin
%{_bindir}/%{name}
%doc tool/MIGRATION.txt
%doc CHANGES.txt README.md
%license LICENSE.txt

%files          help -f .mfiles-javadoc

%changelog
* Wed Mar 04 2020 lihao <lihao129@huawei.com> - 4.5.2-6
- Package Init
